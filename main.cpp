#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <iostream>

#include "include/GameManager.h"
#include "include/TexHolder.h"

double gFrameTime;
SDL_Rect gCamera;
SDL_Renderer* gRenderer;
TexHolder* gTextures;

int main( int argc, char* argv[] )
{
    if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS ) < 0 )
    {
        std::cout << "SDL didn't init \n>" << SDL_GetError() << std::endl;
        return 1;
    }
    if( IMG_Init( IMG_INIT_PNG | IMG_INIT_JPG ) < 0 )
    {
        std::cout << "IMG didn't init \n>" << IMG_GetError() << std::endl;
        return 2;
    }
    if( TTF_Init() < 0 )
    {
        std::cout << "TTF didn't init \n>" << TTF_GetError() << std::endl;
        return 3;
    }

    gCamera = { 0, 0, 1280, 720 };
    SDL_Window* bigWindow = SDL_CreateWindow( "Christmas Frogger", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gCamera.w, gCamera.h,
    //SDL_WINDOW_FULLSCREEN_DESKTOP
    SDL_WINDOW_SHOWN
    );
    //SDL_GetWindowSize( bigWindow, &gCamera.w, &gCamera.h );

    SDL_ShowCursor( false );

    gRenderer = SDL_CreateRenderer( bigWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
    gTextures = new TexHolder;
    GameManager* thegame = new GameManager;

    Uint32 frameStart = SDL_GetTicks();
    while( thegame->get_state() != GameManager::gmsQUITTING )
    {
        gFrameTime = ( SDL_GetTicks() - frameStart ) * 0.001;
        frameStart = SDL_GetTicks();

        thegame->update();
        thegame->draw();

        #define MAX_FPS float( 1000/60 )
        if( SDL_GetTicks() - frameStart < MAX_FPS )
        {
            SDL_Delay( MAX_FPS - ( SDL_GetTicks() - frameStart ) );
        }
    }

    return 0;
}
