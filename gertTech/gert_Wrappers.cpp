#include "gert_Wrappers.h"

#include <SDL2/SDL_image.h>

#include <gert_Collision.h>

#include <cmath>
#include <iostream>

//MULTI FRAME WRAPPING
MultiFrame_Wrap::MultiFrame_Wrap( void )
{
    _myTexture = nullptr;
    _frames = nullptr;
    _rotPoint = nullptr;
    _maxFrames = 0;
}

MultiFrame_Wrap::MultiFrame_Wrap( std::string fPath, Uint8 segWidthNum, Uint8 segHeightNum, Uint16 width, Uint16 height, Uint8 margin, Uint16 customFrameNum )
{
    MultiFrame_Wrap();
    if( segWidthNum != 0 && segHeightNum != 0 )
    {
        if( customFrameNum > segHeightNum * segWidthNum )
        {
            _maxFrames = customFrameNum;
        }
        else
        {
            _maxFrames = segHeightNum * segWidthNum;
        }

        _frames = new SDL_Rect[ _maxFrames ];
        _rotPoint = new SDL_Point[ _maxFrames ];

        for( int y = 0; y < segHeightNum; y++ )
        {
            for( int x = 0; x < segWidthNum; x++ )
            {
                _frames[ x + ( y * segHeightNum ) ] = SDL_Rect{ x*width + margin*x, y*height + margin*y, width, height };
                _rotPoint[ x + ( y * segHeightNum ) ] = { width/2, height/2 };
            }
        }
    }
    else
    {
        _frames = nullptr;
        _rotPoint = nullptr;
    }

    _myTexture = IMG_LoadTexture( gRenderer, fPath.c_str() );
    if( _myTexture == NULL )
    {
        std::cout << "Failed to load texture " << fPath << "\n> " << IMG_GetError() << std::endl;
    }
}

MultiFrame_Wrap::MultiFrame_Wrap( std::string fPath, std::string ssex )
{
    MultiFrame_Wrap();

    SDL_RWops* xmlsheet;
    {
        std::string xmlPath = fPath.substr( 0, fPath.find( "." ) + 1 );
        xmlPath = xmlPath + ssex;
        xmlsheet = SDL_RWFromFile( xmlPath.c_str(), "r" );
    }

    if( xmlsheet != NULL )
    {
        #define READ SDL_RWread( xmlsheet, foo, sizeof( char ), 1 )
        char* foo = new char( 'X' );

        int frameIteration = 0;
        char readingType = 'a';
        bool readval = false;
        while( READ )
        {
            if( !readval )
            {
                switch( *foo )
                {
                case 't':
                case 'x':
                case 'y':
                case 'w':
                case 'h':
                    readval = true;
                    readingType = *foo;
                    break;
                }
            }
            else// failsafe reads
            {
                switch( *foo )
                {
                case ' ':
                case '_':
                case '"':
                    readval = false;
                    readingType = 'a';
                    break;
                }
            }

            if( readval && *foo == '=' )
            {
                READ;
                bool starting = true;
                int totalnum = 0;
                while( starting || *foo != '"' )
                {
                    READ;
                    if( *foo != '"' )
                    {
                        totalnum = totalnum * 10 + ( *foo - '0' );
                    }
                    else
                    {
                        starting = false;
                        readval = false;
                    }
                }

                switch( readingType )
                {
                case 't':
                    _maxFrames = totalnum;
                    _frames = new SDL_Rect[ _maxFrames ];
                    _rotPoint = new SDL_Point[ _maxFrames ];
                    break;
                case 'x':
                    _frames[ frameIteration ].x = totalnum;
                    break;
                case 'y':
                    _frames[ frameIteration ].y = totalnum;
                    break;
                case 'w':
                    _frames[ frameIteration ].w = totalnum;
                    break;
                case 'h':
                    _frames[ frameIteration ].h = totalnum;
                    _rotPoint[ frameIteration ] = { _frames[ frameIteration ].w/2, _frames[ frameIteration ].h/2 };
                    frameIteration += 1;
                    break;
                }
            }
        }

        delete foo;
        SDL_RWclose( xmlsheet );
    }
    else
    {
        std::cout << "Couldn't load sprite sheet data\n";
    }

    _myTexture = IMG_LoadTexture( gRenderer, fPath.c_str() );
    if( _myTexture == NULL )
    {
        std::cout << "Failed to load texture " << fPath << "\n> " << IMG_GetError() << std::endl;
    }
}

bool MultiFrame_Wrap::render( SDL_Rect area, SDL_Color color, int frame, double angle, SDL_RendererFlip flip )
{
    if( collision::get_collide( area, gCamera ) )
    {
        area.x -= gCamera.x;
        area.y -= gCamera.y;

        SDL_Point* rotateAt;
        if( _rotPoint == nullptr )
        {
            rotateAt = NULL;
        }
        else
        {
            rotateAt = &_rotPoint[ frame ];
        }

        SDL_SetTextureColorMod( _myTexture, color.r, color.g, color.b );
        SDL_SetTextureAlphaMod( _myTexture, color.a );
        if( _frames == nullptr )
        {
            SDL_RenderCopyEx( gRenderer, _myTexture, NULL, &area, angle, rotateAt, flip );
        }
        else
        {
            SDL_RenderCopyEx( gRenderer, _myTexture, &_frames[ frame ], &area, angle, rotateAt, flip );
        }
        return true;
    }
    else return false;
}

void MultiFrame_Wrap::enter_custom_frame( int index, SDL_Rect tile, SDL_Point rot )
{
    if( index >= 0 )
    {
        _frames[ index ] = tile;
        if( rot.x != -1 )
        {
            _rotPoint[ index ] = rot;
        }
        else
        {
            _rotPoint[ index ] = { _frames[ index ].w/2, _frames[ index ].h/2 };
        }
    }
}

SDL_Rect MultiFrame_Wrap::get_frame_data( int frame )
{
    if( frame > -1 && frame < _maxFrames && _frames != nullptr )
    {
        return _frames[ frame ];
    }
    return { 0, 0, 0, 0 };
}

MultiFrame_Wrap::~MultiFrame_Wrap( void )
{
    delete[] _frames;
    SDL_DestroyTexture( _myTexture );
}
