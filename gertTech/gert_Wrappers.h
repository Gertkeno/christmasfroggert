#ifndef TEXTURES_H
#define TEXTURES_H

#include <string>
#include <SDL2/SDL.h>

extern SDL_Renderer* gRenderer;
extern SDL_Rect gCamera;

namespace color
{
    const SDL_Color BLACK = { 0, 0, 0, 255 };

    const SDL_Color RED = { 255, 0, 0, 255 };
    const SDL_Color GREEN = { 0, 255, 0, 255 };
    const SDL_Color BLUE = { 0, 0, 255, 255 };

    const SDL_Color YELLOW = { 255, 255, 0, 255 };
    const SDL_Color CYAN = { 0, 255, 255, 255 };
    const SDL_Color MAGENTA = { 255, 0, 255, 255 };

    const SDL_Color WHITE = { 255, 255, 255, 255 };
}

class MultiFrame_Wrap
{
private:
    SDL_Texture* _myTexture;
    SDL_Point* _rotPoint;
    SDL_Rect* _frames;
    int _maxFrames;
public:
    MultiFrame_Wrap( void );
    MultiFrame_Wrap( std::string fPath, Uint8 segWidthNum = 0, Uint8 segHeightNum = 0, Uint16 width = 0, Uint16 height = 0, Uint8 margin = 0, Uint16 customFrameNum = 0 );
    MultiFrame_Wrap( std::string fPath, std::string sheetdatextension );
    bool render( SDL_Rect pos, SDL_Color color, int frame = 0, double a = 0.0, SDL_RendererFlip f = SDL_FLIP_NONE );
    void enter_custom_frame( int, SDL_Rect, SDL_Point rot = { -1, -1 } );

    int get_maxFrames( void ) { return _maxFrames; }
    SDL_Rect get_frame_data( int frame );
    //void set_color( Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha = 0 );

    ~MultiFrame_Wrap( void );
};

#endif // TEXTURES_H
