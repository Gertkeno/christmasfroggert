#include "FreezingObj.h"

#include <TexHolder.h>
#include <gert_Wrappers.h>

#include <SDL2/SDL.h>
#include <SnowBall.h>
#include <gert_Collision.h>

#define MAX_FROZEN_TIME 4 //seconds

SnowBall* FreezingObj::_ice = nullptr;

FreezingObj::FreezingObj( void )
{
    //ctor
    _frozen = false;
    _timeFrozen = 0.0;
}

float FreezingObj::get_deltaX( void )
{
    if( !_frozen )
    {
        return _deltaX;
    }
    return 0;
}

FreezingObj::FreezingObj( SDL_Rect pos, float delta, bool p, int texture, int tile, bool useTexSize )
{
    _frozen = false;
    _timeFrozen = 0.0;
    _wasPainful = _isPainful = p;

    *_drawbox = pos;
    _xBuffer = _drawbox->x;
    _deltaX = delta;

    _texture = texture;
    _tileSetTex = tile;

    if( useTexSize )
    {
        SDL_Rect t = gTextures->mTextures[ _texture ]->get_frame_data( _tileSetTex );
        _drawbox->w = t.w;
        _drawbox->h = t.h;
    }
    _startPos = new SDL_Rect( *_drawbox );
}

void FreezingObj::update( void )
{
    if( _frozen )
    {
        _timeFrozen += gFrameTime;
        if( _timeFrozen > MAX_FROZEN_TIME )
        {
            _frozen = false;
            _timeFrozen = 0.0;
            _isPainful = _wasPainful;
        }
    }
    else
    {
        MovingObj::update();

        if( _ice != nullptr && _ice->alive && collision::get_collide( _ice->assumed_circle(), *_drawbox ) )
        {
            _frozen = true;
            _ice->alive = false;
            _isPainful = false;
        }
    }
}

void FreezingObj::draw( void )
{
    MovingObj::draw();
    if( _frozen )
    {
        SDL_Color alphad = color::WHITE;
        alphad.a = 255 - ( _timeFrozen / MAX_FROZEN_TIME ) * 200;
        int sdiff = ( _timeFrozen / MAX_FROZEN_TIME ) * _drawbox->h;
        SDL_Rect spot = { _drawbox->x, _drawbox->y + sdiff, _drawbox->w, _drawbox->h - sdiff };
        gTextures->mTextures[ TexHolder::thICE ]->render( spot, alphad );
    }
}

void FreezingObj::reset( void )
{
    MovingObj::reset();
    _frozen = false;
    _isPainful = _wasPainful;
    _timeFrozen = 0.0;
}

FreezingObj::~FreezingObj( void )
{
    //dtor
}
