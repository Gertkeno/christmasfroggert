#include "Goal.h"

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

Goal::Goal( void )
{
    //ctor
    _open = true;
}

Goal::Goal( SDL_Rect pos, int tex )
{
    *_drawbox = pos;
    _texture = tex;
    _open = true;
}

void Goal::reset( void )
{
    _open = true;
}

void Goal::draw( void )
{
    #define DARKNESS 80
    SDL_Color c = color::WHITE;
    if( !_open )
    {
        c = { DARKNESS, DARKNESS, DARKNESS, 255 };
    }
    gTextures->mTextures[ _texture ]->render( *_drawbox, c );
}

Goal::~Goal( void )
{
    //dtor
}
