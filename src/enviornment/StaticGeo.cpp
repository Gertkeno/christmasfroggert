#include "StaticGeo.h"

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

StaticGeo::StaticGeo( void )
{
    //ctor
    _isPainful = false;
    _tileSetTex = 0;
    _texture = 0;
}

StaticGeo::StaticGeo( SDL_Rect pos, bool pain, int texture, int tileset )
{
    _tileSetTex = 0;
    _isPainful = pain;
    *_drawbox = pos;
    _texture = texture;
    _tileSetTex = tileset;
}

void StaticGeo::update( void )
{

}

void StaticGeo::draw( void )
{
    #define TEXDATA gTextures->mTextures[ _texture ]->get_frame_data( _tileSetTex )

    if( TEXDATA.w != 0 && TEXDATA.w * 3 < _drawbox->w / 3 )
    {
        for( SDL_Rect tileing = { _drawbox->x, _drawbox->y, TEXDATA.w, TEXDATA.h }; tileing.y < _drawbox->h; tileing.y += tileing.h )
        {
            while( tileing.x < _drawbox->w )
            {
                gTextures->mTextures[ _texture ]->render( tileing, color::WHITE, _tileSetTex );
                tileing.x += tileing.w;
            }
            tileing.x = 0;
        }
    }
    else
    {
        gTextures->mTextures[ _texture ]->render( *_drawbox, color::WHITE, _tileSetTex );
    }
}

StaticGeo::~StaticGeo( void )
{
    //dtor
}
