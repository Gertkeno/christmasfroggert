#include "MovingObj.h"

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

int MovingObj::_leftEndScreen = 0, MovingObj::_rightEndScreen = 0;

MovingObj::MovingObj( void )
{
    //ctor
    _deltaX = 0;
    _xBuffer = 0;
    _leftEndScreen = gCamera.x;
    _rightEndScreen = gCamera.x + gCamera.w;
}

MovingObj::MovingObj( SDL_Rect pos, float delta, bool p, int texture, int tile, bool useTexSize )
{
    _isPainful = p;

    *_drawbox = pos;
    _xBuffer = _drawbox->x;
    _deltaX = delta;

    _texture = texture;
    _tileSetTex = tile;

    if( useTexSize )
    {
        SDL_Rect t = gTextures->mTextures[ texture ]->get_frame_data( tile );
        _drawbox->w = t.w;
        _drawbox->h = t.h;
    }
    _startPos = new SDL_Rect( *_drawbox );
}

void MovingObj::update( void )
{
    _xBuffer += _deltaX * gFrameTime;
    if( _deltaX < 0 && _xBuffer + _drawbox->w < _leftEndScreen ) // negative aka moving left
    {
        _xBuffer = _rightEndScreen;
    }
    else if( _deltaX > 0 && _xBuffer > _rightEndScreen )// moving right
    {
        _xBuffer = _leftEndScreen - _drawbox->w;
    }

    _drawbox->x = _xBuffer;
}

void MovingObj::draw( void )
{
    SDL_RendererFlip f = SDL_FLIP_NONE;
    if( _deltaX < 0 )
    {
        f = SDL_FLIP_HORIZONTAL;
    }
    gTextures->mTextures[ _texture ]->render( *_drawbox, color::WHITE, _tileSetTex, 0, f );
}

void MovingObj::reset( void )
{
    *_drawbox = *_startPos;
    _xBuffer = _drawbox->x;
}

MovingObj::~MovingObj( void )
{
    //dtor
    delete _startPos;
}
