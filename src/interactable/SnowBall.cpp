#include "SnowBall.h"

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>
#include <random>

#define SB_WH 22
#define SB_SPEED ( -450 )

#define ROT_RPS 0.37

SnowBall::SnowBall( void )
{
    //ctor
    alive = false;
    _yBuffer = 0;
    _rot = 0;
    _rotDirection = 1;
}

void SnowBall::start( int posx, int posy )
{
    alive = true;
    *_drawbox = { posx - SB_WH/2, posy - SB_WH/2, SB_WH, SB_WH };
    _yBuffer = _drawbox->y;
    _rotDirection = rand() % 2;
}

void SnowBall::update( void )
{
    if( alive )
    {
        _rot += gFrameTime;

        _yBuffer += SB_SPEED * gFrameTime;
        _drawbox->y = _yBuffer;
        if( _yBuffer + _drawbox->h <= 0 )
        {
            alive = false;
        }
    }
}

void SnowBall::draw( void )
{
    if( alive )
    {
        float angle = ( _rot/ROT_RPS ) * 360;
        if( _rotDirection ) angle *= -1;
        gTextures->mTextures[ TexHolder::thICE ]->render( *_drawbox, color::WHITE, 0, angle );
    }
}

SnowBall::~SnowBall( void )
{
    //dtor
}
