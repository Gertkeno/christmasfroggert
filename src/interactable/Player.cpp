#include "Player.h"

#include <sstream>
#include <iomanip>

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>

#include <Goal.h>

#define IDLE_ANI_CYCLE 0.91
#define IDLE_ANI_FRAMES 4

#define TIRED_MAX_TIME 0.4

#define DYING_ANI_TIME 0.8
#define DYING_ANI_FRAMES 7

//board size 13 x 13

#include <SnowBall.h>
#include <FreezingObj.h>

#include <iostream>//DEBUG

Player::Player( void )
{
    //ctor
    score = 0;
    _ball = new SnowBall;
    FreezingObj::set_pIce( _ball );

    _set_state( psIDLE );
    *_drawbox = { 0, 0, 90, 98 };

    _xBuffer = _drawbox->x;
    _xMovement = 0;
    _yMovement = 0;
}

void Player::start( SDL_Point pos )
{
    _set_state( psTIRED );
    *_drawbox = { pos.x, pos.y, _drawbox->w, _drawbox->h };
    _xBuffer = _drawbox->x;
    _xMovement = 0;
    _yMovement = 0;
}

void Player::interact( const SDL_Event& event )
{
    switch( event.type )
    {
    case SDL_KEYDOWN:
        if( _state == psIDLE )
        {
            switch( event.key.keysym.sym )
            {
            case SDLK_d:
                _set_state( psDYING );//DEBUG
                break;
            case SDLK_SPACE:
                if( !_ball->alive )
                {
                    _ball->start( _drawbox->x + _drawbox->w/2, _drawbox->y + _drawbox->h/2 );
                }
                break;
            case SDLK_LEFT:
                _yMovement = 0;
                _xMovement = -1;
                break;
            case SDLK_RIGHT:
                _yMovement = 0;
                _xMovement = 1;
                break;
            case SDLK_UP:
                _xMovement = 0;
                _yMovement = -1;
                break;
            case SDLK_DOWN:
                _xMovement = 0;
                _yMovement = 1;
                break;
            }
        }
        break; //end KEYDOWN
    case SDL_KEYUP:
        switch( event.key.keysym.sym )
        {
        case SDLK_LEFT:
        case SDLK_RIGHT:
            _xMovement = 0;
            break;
        case SDLK_UP:
        case SDLK_DOWN:
            _yMovement = 0;
            break;
        }
        break;//end KEYUP
    }//end switch event type
}

void Player::update( void )
{
    #define SCORE_DECAY 10
    _timeInState += gFrameTime;
    switch( _state )
    {
    case psIDLE:
        score -= SCORE_DECAY * gFrameTime;
        //MOVEMENT
        #define MOVEDISTANCE 120.0
        if( _xMovement != 0 || _yMovement != 0 )
        {
            bool proc = false;
            if( _drawbox->x + _xMovement * MOVEDISTANCE >= gCamera.x && _drawbox->x + _drawbox->w/2 + _xMovement * MOVEDISTANCE <= gCamera.w + gCamera.x )
            {
                _drawbox->x = ( int( float( _drawbox->x ) / MOVEDISTANCE + 0.4 ) + _xMovement ) * MOVEDISTANCE; //todid: make movement go 50/50 when offset
                proc = true;
            }
            if( _drawbox->y + _yMovement * MOVEDISTANCE >= gCamera.y && _drawbox->y + _drawbox->h/2 + _yMovement * MOVEDISTANCE <= gCamera.h + gCamera.y )
            {
                score -= _yMovement * 500;
                _drawbox->y = ( _drawbox->y / MOVEDISTANCE + _yMovement ) * MOVEDISTANCE;
                proc = true;
            }

            if( proc )
            {
                _xBuffer = _drawbox->x;
                _set_state( psTIRED );
            }
            //std::cout << _drawbox->x << ", " << _drawbox->y << std::endl; //DEBUG
        }
        //ANIMATION
        if( _timeInState >= IDLE_ANI_CYCLE )
        {
            _timeInState = 0;
        }
        break;
    case psTIRED:
        score -= SCORE_DECAY * gFrameTime;
        if( _timeInState >= TIRED_MAX_TIME )
        {
            _set_state( psIDLE );
        }
        break;
    case psDYING:
        if( _timeInState >= DYING_ANI_TIME )
        {
            _set_state( psDEAD );
            _ball->alive = false;
        }
        break;
    case psDEAD:
        _timeInState = 0;
        break;
    }
    _ball->update();
}

void Player::draw( void )
{
    #define DARKER_COLOR 150
    SDL_Color responsive = color::WHITE;
    switch( _state )
    {
    case psTIRED:
        responsive = { DARKER_COLOR, DARKER_COLOR, DARKER_COLOR, 255 };
    case psIDLE:
        gTextures->mTextures[ TexHolder::thPLAYER ]->render( *_drawbox, responsive, int( ( _timeInState / IDLE_ANI_CYCLE ) * IDLE_ANI_FRAMES ) % IDLE_ANI_FRAMES );
        _ball->draw();
        break;
    case psDYING:
        gTextures->mTextures[ TexHolder::thPLAYER]->render( *_drawbox, color::WHITE, int( ( _timeInState/DYING_ANI_TIME ) * DYING_ANI_FRAMES ) % DYING_ANI_FRAMES + IDLE_ANI_FRAMES );
        break;
    case psDEAD:
        break;
    }

    SDL_Rect roundEdge = { 0, 0, gTextures->mTextures[ TexHolder::thMENU ]->get_frame_data( 12 ).w, gTextures->mFont->string_height() };
    std::stringstream ss; ss <<std::setprecision( 0 ) << std::fixed << "Score: " << score;
    SDL_Rect spot = { roundEdge.x + roundEdge.w, 0, gTextures->mFont->string_width( ss.str() ), gTextures->mFont->string_height() };
    gTextures->mTextures[ TexHolder::thMENU ]->render( spot, color::WHITE, 13 );
    gTextures->mTextures[ TexHolder::thMENU ]->render( roundEdge, color::WHITE, 12 );
    roundEdge.x = spot.x + spot.w;
    gTextures->mTextures[ TexHolder::thMENU ]->render( roundEdge, color::WHITE, 14 );
    gTextures->mFont->render( spot, ss.str(), color::BLACK );
}

bool Player::check_collide( MovingObj* tohit )
{
    if( _state == psIDLE || _state == psTIRED )
    {
        bool goingToDie = check_collide( (StaticGeo*)( tohit ) );

        if( !goingToDie && collision::get_collide( assumed_circle(), tohit->get_drawbox() ) )
        {
            _xBuffer += tohit->get_deltaX() * gFrameTime;
            if( _xBuffer > gCamera.x && _xBuffer < gCamera.x + gCamera.w )
            {
                _drawbox->x = _xBuffer;
            }
            else
            {
                _xBuffer = _drawbox->x;
            }
            return true;
        }
        return goingToDie;
    }
    return false;
}

bool Player::check_collide( StaticGeo* tohit )
{
    if( ( _state == psIDLE || _state == psTIRED ) && tohit->get_painful() && collision::get_collide( assumed_circle(), tohit->get_drawbox() ) )
    {
        _set_state( psDYING );
        return true;
    }
    return false;
}

bool Player::check_collide( Goal* tohit )
{
    if( ( _state == psIDLE || _state == psTIRED ) && tohit->_open && collision::get_collide( assumed_circle(), tohit->get_drawbox() ) )
    {
        score += 2000;
        tohit->_open = false;
        return true;
    }
    return false;
}

void Player::_set_state( status s )
{
    _state = s;
    _timeInState = 0;
}

Player::~Player( void )
{
    //dtor
    delete _ball;
}
