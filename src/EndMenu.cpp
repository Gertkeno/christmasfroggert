#include "EndMenu.h"

#include <sstream>

#include <SDL2/SDL.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>
#include <gert_CameraMath.h>

#define MAX_HIGHSCORES 12

#define str_NEW_GAME "New Game"
#define str_HIGH_SCORES "High Scores"
#define str_QUIT "Quit"

#define SHEIGHT gTextures->mFont->string_height()

#define MAKE_OPTION( index, sting )\
    _clickbox[ index ] = rectAlign::middle( { 0, y, gTextures->mFont->string_width( sting ), SHEIGHT }, gCamera ); y += SHEIGHT + 2;

EndMenu::EndMenu( void )
{
    //ctor
    _currentHighscore = -1;
    _HSview = false;
    _currentSelect = oNEW_GAME;
    _clickbox = new SDL_Rect[ oTOTAL_OPTIONS ];
    int y = SHEIGHT;

    MAKE_OPTION( oNEW_GAME, str_NEW_GAME )
    MAKE_OPTION( oHIGHSCORES, str_HIGH_SCORES )
    MAKE_OPTION( oQUIT, str_QUIT )

    _highscores = new int[ MAX_HIGHSCORES ];

    SDL_RWops* hsFile = SDL_RWFromFile( "highscores.dat", "rb" );
    if( hsFile != NULL )
    {
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            SDL_RWread( hsFile, &_highscores[ i ], 1, sizeof( int ) );
        }
        SDL_RWclose( hsFile );
    }
    else
    {
        hsFile = SDL_RWFromFile( "highscores.dat", "w" );
        int foo = 1;
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            _highscores[ i ] = foo;
            SDL_RWwrite( hsFile, &foo, sizeof( int ), 1 );
        }

        SDL_RWclose( hsFile );
    }
}

#define PUTS( index, string ) gTextures->mFont->render( _clickbox[ index ], string, color::BLACK );

void EndMenu::draw( void )
{
    if( !_HSview )
    {
        for( int i = 0; i < oTOTAL_OPTIONS; i++ )
        {
            SDL_Color c = color::WHITE;
            if( _currentSelect == i )
            {
                c = color::GREEN;
            }
            SDL_Rect roundEdge = { 0, _clickbox[ i ].y, 0, _clickbox[ i ].h };
            roundEdge.w = gTextures->mTextures[ TexHolder::thMENU ]->get_frame_data( 12 ).w;
            roundEdge.x = _clickbox[ i ].x - roundEdge.w;
            gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( _clickbox[ i ] ), c, 13 );
            gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), c, 12 );

            roundEdge.x = _clickbox[ i ].x + _clickbox[ i ].w;
            gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), c, 14 );
        }

        PUTS( oNEW_GAME, str_NEW_GAME )
        PUTS( oHIGHSCORES, str_HIGH_SCORES )
        PUTS( oQUIT, str_QUIT )
    }
    else
    {
        SDL_Rect bg = rectAlign::middle( { 0, 0, 400, ( MAX_HIGHSCORES + 2 ) * SHEIGHT }, gCamera );
        gTextures->mTextures[ TexHolder::thMENU ]->render( bg, color::WHITE, 63 );
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            std::stringstream sshs; sshs << i+1 << "- " << _highscores[ i ];
            SDL_Rect pos = rectAlign::middle( { 0, i * SHEIGHT + 6, gTextures->mFont->string_width( sshs.str() ), SHEIGHT }, bg );
            if( _currentHighscore == i )
            {
                gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( pos ), color::YELLOW, 13 );
                SDL_Rect roundEdge = { 0, pos.y, 0, pos.h };
                roundEdge.w = gTextures->mTextures[ TexHolder::thMENU ]->get_frame_data( 12 ).w;
                roundEdge.x = pos.x - roundEdge.w;
                gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), color::YELLOW, 12 );
                roundEdge.x = pos.x + pos.w;
                gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), color::YELLOW, 14 );
            }
            gTextures->mFont->render( pos, sshs.str(), color::BLACK );
        }
        #define str_BACK "Back"
        SDL_Rect ok = rectAlign::middle( { 0, MAX_HIGHSCORES * SHEIGHT, gTextures->mFont->string_width( str_BACK ), SHEIGHT }, gCamera );
        gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( ok ), color::GREEN, 13 );
        gTextures->mFont->render( add_camera( ok ), str_BACK, color::BLACK );
        SDL_Rect roundEdge = { 0, ok.y, 0, ok.h };
        roundEdge.w = gTextures->mTextures[ TexHolder::thMENU ]->get_frame_data( 12 ).w;
        roundEdge.x = ok.x - roundEdge.w;
        gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), color::GREEN, 12 );
        roundEdge.x = ok.x + ok.w;
        gTextures->mTextures[ TexHolder::thMENU ]->render( add_camera( roundEdge ), color::GREEN, 14 );
    }
}

void EndMenu::push_highscore( const float& score )
{
    bool aBigOne = false;
    int core = int( score + 0.5 );
    for( int i = 0; i < MAX_HIGHSCORES; i++ )
    {
        if( core > _highscores[ i ] )
        {
            for( int a = MAX_HIGHSCORES - 1; a > i ; a-- )
            {
                _highscores[ a ] = _highscores [ a - 1 ];
            }
            _highscores[ i ] = core;
            _currentHighscore = i;
            aBigOne = true;
            break;
        }
    }

    if( !aBigOne )
    {
        _currentHighscore = -1;
    }
}

void EndMenu::write_highscores( void )
{
    SDL_RWops* hsFile = SDL_RWFromFile( "highscores.dat", "w" );
    if( hsFile != NULL )
    {
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            SDL_RWwrite( hsFile, &_highscores[ i ], sizeof( int ), 1 );
        }

        SDL_RWclose( hsFile );
    }
}

EndMenu::options EndMenu::interact( const SDL_Event& event )
{
    if( !_HSview )
    {
        switch( event.type )
        {
        case SDL_KEYDOWN:
            switch( event.key.keysym.sym )
            {
            case SDLK_DOWN:
                _currentSelect += 1;
                if( _currentSelect >= oTOTAL_OPTIONS )
                {
                    _currentSelect = 0;
                }
                break;
            case SDLK_UP:
                _currentSelect -= 1;
                if( _currentSelect < 0 )
                {
                    _currentSelect = oTOTAL_OPTIONS - 1;
                }
                break;
            case SDLK_SPACE:
                if( _currentSelect == oHIGHSCORES )
                {
                    _HSview = true;
                }
                return options( _currentSelect );
                break;
            }
        }
    }
    else
    {
        switch( event.type )
        {
        case SDL_KEYDOWN:
            switch( event.key.keysym.sym )
            {
            case SDLK_SPACE:
                _HSview = false;
                break;
            }
        }
    }

    return oTOTAL_OPTIONS;
}

EndMenu::~EndMenu( void )
{
    //dtor
    delete[] _clickbox;
}
