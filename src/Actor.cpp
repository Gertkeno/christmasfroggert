#include "Actor.h"

#include <SDL2/SDL.h>
#include <gert_Collision.h>

Actor::Actor( void )
{
    //ctor
    _drawbox = new SDL_Rect{ 0, 0, 0, 0 };
}

Circle Actor::assumed_circle( void )
{
    Circle re = { float( _drawbox->x + _drawbox->w/2 ), float( _drawbox->y + _drawbox->h/2 ), 0 };
    if( _drawbox->w > _drawbox->h )
    {
        re.r = _drawbox->w/2;
    }
    else
    {
        re.r = _drawbox->h/2;
    }

    return re;
}

SDL_Rect Actor::get_drawbox( void ) { return *_drawbox; }

Actor::~Actor( void )
{
    //dtor
    delete _drawbox;
}
