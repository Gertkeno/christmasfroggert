#include "GameManager.h"

#include <SDL2/SDL.h>
#include <Player.h>

#include <FreezingObj.h>
#include <Goal.h>
#include <TexHolder.h>
#include <gert_Wrappers.h>

#include <EndMenu.h>

#define MAX_MOVING_OBJS 6
#define MAX_GOALS 6

GameManager::GameManager( void )
{
    //ctor
    _state = gmsINMENU;
    _event = new SDL_Event;

    _guy = new Player;
    _guy->start( { 600, 600 } );

    _river = new StaticGeo( { 0, 0, gCamera.w, 200 }, true, TexHolder::thBGSET, 199 );

    _hazard = new MovingObj*[ MAX_MOVING_OBJS ];
    int i = 0;
    _hazard[ i++ ] = new FreezingObj( { 0, 340, 113, 80 }, 350, true, TexHolder::thCARS, 5, true );
    _hazard[ i++ ] = new FreezingObj( { gCamera.w, 250, 200, 80 }, -700, true, TexHolder::thCARS, 8, true );
    _hazard[ i++ ] = new MovingObj( { 0, 145, 300, 50 }, -200, false, TexHolder::thLOG );
    _hazard[ i++ ] = new FreezingObj( { 0, 470, 10, 10 }, -100, true, TexHolder::thCARS, 13, true );
    _hazard[ i++ ] = new FreezingObj( { 900, 470, 10, 10 }, -100, true, TexHolder::thCARS, 9, true );
    _hazard[ i++ ] = new FreezingObj( { 600, 120, 93, 90 }, 0, true, TexHolder::thCROC );

    _goals = new Goal*[ MAX_GOALS ];
    for( int i = 0; i < MAX_GOALS; i++ )
    {
        _goals[ i ] = new Goal( { i * gCamera.w/MAX_GOALS, 0, 96, 68 }, TexHolder::thIGLOO );
    }

    _menu = new EndMenu;
}

void GameManager::update( void )
{
    while( SDL_PollEvent( _event ) )
    {
        switch( _state ) //STATE SPECIFIER
        {
        case gmsPLAYING:
            _guy->interact( *_event );
            break;
        case gmsINMENU:
        case gmsENDGAME:
            switch( _menu->interact( *_event ) )
            {
            case EndMenu::oNEW_GAME:
                _state = gmsPLAYING;
                _reseting();
                break;
            case EndMenu::oQUIT:
                _state = gmsQUITTING;
                break;
            case EndMenu::oHIGHSCORES:
                break;
            case EndMenu::oTOTAL_OPTIONS:
                break;
            }
            break;
        case gmsQUITTING:
            break;
        }//end state specifier

        switch( _event->type ) //ALWAYS ACTIONS
        {
        case SDL_KEYDOWN:
            switch( _event->key.keysym.sym )
            {
            case SDLK_F4:
                _state = gmsQUITTING;
                break;
            case SDLK_ESCAPE:
                _state = gmsINMENU;
                break;
            }
            break;//end keydown
        }// end global actions
    }

    bool allClosed = true;
    bool riversafe = false;
    switch( _state ) // constant updates
    {
    case gmsPLAYING:
        for( int i = 0; i < MAX_MOVING_OBJS; i++ )
        {
            _hazard[ i ]->update();
        }
        _river->update();
        _guy->update();

        allClosed = true;
        for( int i = 0; i < MAX_GOALS; i++ )
        {
            if( _guy->check_collide( _goals[ i ] ) )
            {
                _guy->start( { 600, 600 } );
            }
            if( _goals[ i ]->is_open() )
            {
                allClosed = false;
            }
        }
        if( allClosed )
        {
            _state = gmsENDGAME;
            _guy->score += 6000;
            _menu->push_highscore( _guy->score );
        }

        //Collisons
        riversafe = false;
        for( int i = 0; i < MAX_MOVING_OBJS; i++ )
        {
            if( _guy->check_collide( _hazard[ i ] ) )
            {
                riversafe = true;
            }
        }
        if( !riversafe )
        {
            _guy->check_collide( _river );
        }

        if( _guy->is_dead() )
        {
            _state = gmsENDGAME;
            _menu->push_highscore( _guy->score );
        }
        break;
    case gmsINMENU:
        break;
    case gmsENDGAME:
        break;
    case gmsQUITTING:
        _menu->write_highscores();
        break;
    }
}

void GameManager::draw( void )
{
    SDL_RenderClear( gRenderer );
    SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );

    for( SDL_Rect bgSpot = { 0, 0, 32, 32 }; bgSpot.y <= gCamera.h; bgSpot.y += bgSpot.h )
    {
        while( bgSpot.x < gCamera.w )
        {
            gTextures->mTextures[ TexHolder::thBGSET ]->render( bgSpot, color::WHITE, 135 );
            bgSpot.x += bgSpot.w;
        }
        bgSpot.x = 0;
    }

    switch( _state )
    {
    case gmsENDGAME:
    case gmsPLAYING:
        _river->draw();
        for( int i = 0; i < MAX_MOVING_OBJS; i++ )
        {
            _hazard[ i ]->draw();
        }
        for( int i = 0; i < MAX_GOALS; i++ )
        {
            _goals[ i ]->draw();
        }
        _guy->draw();
        break;
    case gmsINMENU:
        break;
    case gmsQUITTING:
        break;
    }//end gamestate switch

    if( _state == gmsINMENU || _state == gmsENDGAME )
    {
        _menu->draw();
    }

    SDL_RenderPresent( gRenderer );
}

void GameManager::_reseting( void )
{
    for( int i = 0; i < MAX_MOVING_OBJS; i++ )
    {
        _hazard[ i ]->reset();
    }
    for( int i = 0; i < MAX_GOALS; i++ )
    {
        _goals[ i ]->reset();
    }
    _guy->start( { 600, 600 } );
    _guy->score = 0;
}

GameManager::~GameManager( void )
{
    //dtor
    for( int i = 0; i < MAX_MOVING_OBJS; i++ )
    {
        delete _hazard[ i ];
    }
    delete[] _hazard;
    delete _guy;
    delete _river;
    delete _event;
}
