#include "TexHolder.h"

#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>

TexHolder::TexHolder( void )
{
    //ctor
    mTextures = new MultiFrame_Wrap*[ thTEXTURES_TOTAL ];

    mTextures[ thPLAYER ] = new MultiFrame_Wrap( "assets/playa.png", "xml" );
    mTextures[ thICE ] = new MultiFrame_Wrap( "assets/iced.png" );
    mTextures[ thCIRCLE ] = new MultiFrame_Wrap( "assets/circle.png" );
    mTextures[ thCARS ] = new MultiFrame_Wrap( "assets/cars.png", "xml" );
    mTextures[ thLOG ] = new MultiFrame_Wrap( "assets/log.png" );
    mTextures[ thMENU ] = new MultiFrame_Wrap( "assets/uipackSpace_sheet.png", "xml" );
    mTextures[ thIGLOO ] = new MultiFrame_Wrap( "assets/Igloo.png" );
    mTextures[ thBGSET ] = new MultiFrame_Wrap( "assets/roguelikeDungeon_transparent.png", 29, 18, 16, 16, 1 );
    mTextures[ thCROC ] = new MultiFrame_Wrap( "assets/croc.png" );

    mFont = new Font_Wrapper( "OptimusPrinceps.ttf", 24 );
}

TexHolder::~TexHolder( void )
{
    //dtor
    for( int i = 0; i < thTEXTURES_TOTAL; i++ )
    {
        delete mTextures[ i ];
    }
    delete[] mTextures;

    delete mFont;
}
