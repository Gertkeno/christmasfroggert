#ifndef ACTOR_H
#define ACTOR_H

class TexHolder;

extern TexHolder* gTextures;
extern double gFrameTime;

struct SDL_Rect;
struct Circle;

class Actor
{
    public:
        Actor( void );

        Circle assumed_circle( void );
        SDL_Rect get_drawbox( void );

        virtual void update( void ) = 0;
        virtual void draw( void ) = 0;

        virtual ~Actor( void );
    protected:
        SDL_Rect* _drawbox;
};

#endif // ACTOR_H
