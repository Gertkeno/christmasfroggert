#ifndef PLAYER_H
#define PLAYER_H

#include <Actor.h>

union SDL_Event;
struct SDL_Point;
class SnowBall;
class MovingObj;
class StaticGeo;

class Goal;

class Player : public Actor
{
    public:
        Player( void );

        void start( SDL_Point pos );
        void interact( const SDL_Event& event );

        void update( void );
        void draw( void );

        bool is_dead( void ) { return ( _state == psDEAD ); }

        bool check_collide( MovingObj* tohit );
        bool check_collide( StaticGeo* tohit );
        bool check_collide( Goal* tohit );

        float score;

        virtual ~Player( void );
    private:
        int _xMovement;
        float _xBuffer;
        int _yMovement;
        SnowBall* _ball;

        enum status
        {
            psIDLE,
            psTIRED,
            psDYING,
            psDEAD
        };
        status _state;
        float _timeInState;

        void _set_state( status s );
};

#endif // PLAYER_H
