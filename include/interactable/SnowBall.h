#ifndef SNOWBALL_H
#define SNOWBALL_H

#include <Actor.h>


class SnowBall : public Actor
{
    public:
        SnowBall( void );

        void start( int startingPositionX, int startingPositionY );

        bool alive;

        virtual void update( void );
        virtual void draw( void );

        virtual ~SnowBall( void );
    private:
        float _yBuffer;
        float _rot;
        bool _rotDirection;
};

#endif // SNOWBALL_H
