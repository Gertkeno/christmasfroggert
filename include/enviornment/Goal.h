#ifndef GOAL_H
#define GOAL_H

#include <StaticGeo.h>

class Player;

class Goal : public StaticGeo
{
    public:
        friend class Player;
        Goal( void );
        Goal( SDL_Rect pos, int texture );

        bool is_open( void ) { return _open; }
        void reset( void );

        void draw( void );

        virtual ~Goal( void );
    private:
        bool _open;
};

#endif // GOAL_H
