#ifndef MOVINGOBJ_H
#define MOVINGOBJ_H

#include <StaticGeo.h>


class MovingObj : public StaticGeo
{
    public:
        MovingObj( void );
        MovingObj( SDL_Rect pos, float delta, bool pain, int texture, int tile = 0, bool useTexSize = false );

        virtual void update( void );
        virtual void draw( void );

        virtual void reset( void );

        virtual float get_deltaX( void ) { return _deltaX; }

        virtual ~MovingObj( void );
    protected:
        static int _leftEndScreen;
        static int _rightEndScreen;
        float _xBuffer;
        float _deltaX;

        SDL_Rect* _startPos;
};

#endif // MOVINGOBJ_H
