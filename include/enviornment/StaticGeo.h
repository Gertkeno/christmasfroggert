#ifndef STATICGEO_H
#define STATICGEO_H

#include <Actor.h>

struct SDL_Rect;

class StaticGeo : public Actor
{
    public:
        StaticGeo( void );
        StaticGeo( SDL_Rect pos, bool painful, int texture, int tileset = 0 );

        bool get_painful( void ) { return _isPainful; }

        virtual void update( void );
        virtual void draw( void );

        virtual ~StaticGeo( void );
    protected:
        int _texture;
        int _tileSetTex;
        bool _isPainful;
};

#endif // STATICGEO_H
