#ifndef FREEZINGOBJ_H
#define FREEZINGOBJ_H

#include <MovingObj.h>

class SnowBall;

class FreezingObj : public MovingObj
{
    public:
        FreezingObj( void );
        FreezingObj( SDL_Rect pos, float deltax, bool painful, int texture, int tile = 0, bool useTexSize = false );

        virtual void update( void );
        virtual void draw( void );

        float get_deltaX( void );

        virtual void reset( void );

        static void set_pIce( SnowBall* b ) { _ice = b; }

        virtual ~FreezingObj( void );
    protected:
        bool _frozen;
        float _timeFrozen;

        bool _wasPainful;

        static SnowBall* _ice;
};

#endif // FREEZINGOBJ_H
