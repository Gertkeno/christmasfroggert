#ifndef ENDMENU_H
#define ENDMENU_H

struct SDL_Rect;
union SDL_Event;

class TexHolder;
extern TexHolder* gTextures;

class EndMenu
{
    public:
        EndMenu( void );

        enum options
        {
            oNEW_GAME,
            oHIGHSCORES,
            oQUIT,
            oTOTAL_OPTIONS
        };

        options interact( const SDL_Event& event );
        void draw( void );

        void push_highscore( const float& score );
        void write_highscores( void );

        virtual ~EndMenu( void );
    private:
        SDL_Rect *_clickbox;
        int *_highscores;
        int _currentHighscore;
        bool _HSview;
        int _currentSelect;
};

#endif // ENDMENU_H
