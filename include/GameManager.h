#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

union SDL_Event;
class SDL_Renderer;
struct SDL_Rect;

class StaticGeo;
class MovingObj;
class Goal;

class Player;

class EndMenu;

extern SDL_Rect gCamera;
extern SDL_Renderer* gRenderer;

class GameManager
{
    public:
        enum gmstate
        {
            gmsPLAYING,
            gmsENDGAME,
            gmsQUITTING,
            gmsINMENU
        };

        gmstate get_state( void ) { return _state; }

        void update( void );
        void draw( void );

        GameManager( void );
        virtual ~GameManager( void );
    private:
        gmstate _state;
        SDL_Event* _event;

        Player* _guy;

        StaticGeo* _river;
        MovingObj** _hazard;

        Goal** _goals;
        void _reseting( void );

        EndMenu* _menu;
};

#endif // GAMEMANAGER_H
