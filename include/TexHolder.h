#ifndef TEXHOLDER_H
#define TEXHOLDER_H

class MultiFrame_Wrap;
class Font_Wrapper;

class TexHolder
{
    public:
        TexHolder( void );

        enum texnames
        {
            thPLAYER,
            thICE,
            thCIRCLE,
            thCARS,
            thLOG,
            thMENU,
            thIGLOO,
            thBGSET,
            thCROC,
            thTEXTURES_TOTAL
        };
        MultiFrame_Wrap** mTextures;

        Font_Wrapper* mFont;

        virtual ~TexHolder( void );
};

#endif // TEXHOLDER_H
